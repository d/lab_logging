# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 15:22:00 2019

@author: Tebbe
"""

import tkinter as tk
import tkinter.ttk as ttk

import numpy as np
import datetime

#Declaring Variables
import configparser
conf_file_loc='Log/configfile_for_log.ini'
config = configparser.ConfigParser()
config.read(conf_file_loc)


def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top

def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root, windowopen
    w = None
    windowopen=1
    root = tk.Tk()
    top = Lab_log (root)
    init(root, top)
    #monitoring_with_Senty_Thread().start()
    #monitoring_with_Senty_Thread().setDaemon(True)
    root.mainloop()
    if top.working:
        top.working=False
        top.end_time=datetime.datetime.now()
        top.write_file(top.working_user)
    windowopen=0
    print('windowclosed')

def create_New_Toplevel_1(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Lab_log (root)
    #set_Tk_var()
    top = Lab_log (w)
    init(w, top, *args, **kwargs)
    return (w, top)

def destroy_New_Toplevel_1():
    global w
    w.destroy()
    w = None

def _quit():
    global windowopen
    windowopen=0
    print('windowclosed')
    root.quit()     # stops mainloop
    root.destroy()

class Lab_log():
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana2color = '#d9d9d9' # X11 color: 'gray85'
        
       
        self.working=False
        self.start_time=0
        self.end_time=0
        self.savefile=config.get('admin settings','name_of_Logfile')
        
        self.list_users = [key for key in config['list_of_users']]
        self.new_user=True
        self.working_user='select_user'
        self.combobox_user = tk.StringVar()
        self.combobox_user.set('select_user')
        
        self.StartStop=tk.StringVar()
        self.StartStop.set('System idle\npress here to Start')

        #pending printing is the list which gets emptied by printing its Content in the Info Field
        self.pending_printing=['Startup']
        
        #text Variable in Info Box
        self.textinTLabelProcess_Log=tk.StringVar()
        #text to be printed in Info Box
        self.printed_text_to_display=['Ready Player One']
        #Scrollbar in Info Box
        self.TScale2Scrollbar=tk.DoubleVar()
        self.TScale2Scrollbar.set(0)
        
        #Set ttk Stile
        self.style = ttk.Style()
        self.style.theme_use('xpnative')
        self.style.configure('.',background=_bgcolor)
        self.style.configure('.',foreground=_fgcolor)
        self.style.configure('.',font="TkDefaultFont")
        self.style.map('.',background=[('selected', _compcolor), ('active',_ana2color)])

        top.geometry("280x450+100+100")
        top.title(config.get('admin settings','name_of_programm'))
        
        #Buttons on the top left
        self.relxbutton=0.02
        self.widthbutton=250
        
        self.TLabelframe_Buttons = ttk.Labelframe(top)
        self.TLabelframe_Buttons.place(relx=0.01, rely=0.01, relheight=0.64, relwidth=0.98)
        self.TLabelframe_Buttons.configure(text='Annealing Prozess')
        
        
        #self.style.configure("TButton", background='black', foreground='black')
        
        self.style.configure("RB.TButton", background='green', foreground='green', font=('Helvetica', 12))
        
        self.Start_Stop = ttk.Button(self.TLabelframe_Buttons, style="RB.TButton")
        self.Start_Stop.place(relx=self.relxbutton, rely=0.02, height=100, width=self.widthbutton)
        self.Start_Stop.configure(takefocus="")
        self.Start_Stop.configure(command=self.start_stop)
        self.Start_Stop.configure(textvariable=self.StartStop)
        self.Start_Stop.configure(width=133)
        
        
        #self.DeleteUser = ttk.Button(self.TLabelframe_Buttons)
        #self.DeleteUser.place(relx=self.relxbutton, rely=0.77, height=45, width=self.widthbutton)
        #self.DeleteUser.configure(takefocus="")
        #self.DeleteUser.configure(command=self.delete_current_User)
        #self.DeleteUser.configure(text="Delete currently selected User")
        #self.DeleteUser.configure(width=133)
        
        #User Selection Entry Box
        self.TLabelframe_Choose_User = ttk.Labelframe(self.TLabelframe_Buttons)
        self.TLabelframe_Choose_User.place(relx=self.relxbutton, rely=0.52, height=45, width=self.widthbutton)
        self.TLabelframe_Choose_User.configure(text='Choose your Username:')
        
        self.combobox_user.trace_variable('w',self.combobox_user_canged)
        
        self.TCombobox_Choose_User = ttk.Combobox(self.TLabelframe_Choose_User)
        self.TCombobox_Choose_User.place(relx=0, rely=0, relheight=1, relwidth=1)
        self.TCombobox_Choose_User.configure(textvariable=self.combobox_user)
        self.TCombobox_Choose_User.configure(values=self.list_users)
        self.TCombobox_Choose_User.configure(takefocus="")
        
        #Info Box
        self.TLabelframe_Process = ttk.Labelframe(top)
        self.TLabelframe_Process.place(relx=0.01, rely=0.67, relheight=0.3, relwidth=0.98)
        self.TLabelframe_Process.configure(text='Process and Info')
        
        self.TLabelProcess_Log = ttk.Label(self.TLabelframe_Process)
        self.TLabelProcess_Log.place(relx=0, rely=0, relheight=1, relwidth=0.9)
        self.TLabelProcess_Log.configure(background="#d9d9d9")
        self.TLabelProcess_Log.configure(foreground="#000000")
        self.TLabelProcess_Log.configure(relief=tk.FLAT)
        self.TLabelProcess_Log.configure(textvariable=self.textinTLabelProcess_Log)
        self.TLabelProcess_Log.configure(width=500)
        
        self.TScale2 = ttk.Scale(self.TLabelframe_Process, orient=tk.VERTICAL, variable=self.TScale2Scrollbar, from_=0, to=1)
        self.TScale2.place(relx=0.95, rely=0, relwidth=0.1, relheight=1, height=0)
        self.TScale2.configure(takefocus="")
        
        self.apendProcess_Log()
    
    def start_stop(self):
        if self.working:
            self.working=False
            self.end_time=datetime.datetime.now()
            self.write_file(self.working_user)
            self.pending_printing.append('User '+self.working_user+' stoped working')
            self.StartStop.set('System idle\npress here to Start')
            
            self.style.configure("RB.TButton", background='green', foreground='green', font=('Helvetica', 12))
        
        else:
            if self.combobox_user.get() in ['select_user','',' ']:
                self.pending_printing.append("No User Selected")
            else:
                self.working=True
                self.working_user=self.combobox_user.get()
                if self.new_user:
                    self.list_users.append(self.working_user)
                    self.write_conf_File()
                self.start_time=datetime.datetime.now()
                self.pending_printing.append('User '+self.working_user+' started working')
                self.StartStop.set('System in use by\n'+ self.working_user+'\npress here to Stop')
                self.style.configure("RB.TButton", background='red', foreground='red', font=('Helvetica', 15) )
        self.apendProcess_Log()
    
    def delete_current_User(self):
        if self.working:
            self.pending_printing.append('User '+self.working_user+' is currently working')
        else:
            if (self.combobox_user.get() in self.list_users):
                todel=self.combobox_user.get()
                self.combobox_user.set('select_user')
                self.working_user='select_user'
                del self.list_users[self.list_users.index(todel)]
                self.write_conf_File()
            self.pending_printing.append('Selected User '+self.combobox_user.get()+' deleted')
        self.apendProcess_Log()
         
    #Selectioon of a different User
    def combobox_user_canged(self,name,b,action):
        if (self.combobox_user.get() in self.list_users):
            self.new_user=False
        else:
            self.new_user=True
    
    #Print into the Info Box
    def apendProcess_Log(self):
        listing=self.pending_printing
        self.pending_printing=[]
        listing.reverse()
        self.printed_text_to_display = [datetime.datetime.now().strftime("%H:%M")+' '+li for li in listing] + self.printed_text_to_display 
        linetostart=int(len(self.printed_text_to_display)*self.TScale2Scrollbar.get())-10 #10 is number of lines are diplayed if slider is all the way down
        if(linetostart<0):
            linetostart=0
        self.textinTLabelProcess_Log.set(self.printed_text_to_display[linetostart])
        for line in self.printed_text_to_display[linetostart+1:]:
            self.textinTLabelProcess_Log.set(self.textinTLabelProcess_Log.get()+'\n'+str(line))
    
    #Write the Configuration File, with Programms
    def write_conf_File(self):
        config['list_of_users']={self.order(self.list_users)[i] : str(0) for i in range(len(self.list_users))}   
        with open(conf_file_loc, 'w') as configfile:
            config.write(configfile)
     
    def write_file(self, name):
        form="%Y.%m.%d\t%H:%M:%S"
        delta=self.end_time-self.start_time
        with open(self.savefile, 'a') as file:
            file.write(name+'\t'+self.start_time.strftime(form)+'\t'+self.end_time.strftime(form)+'\t'+str(delta)+'\n')
            
    def order(self,l):
        temp= []
        for xi in sorted(l):
            temp.append(xi)
        return temp

'''
class monitoring_with_Senty_Thread(threading.Thread):
    def run(self):
        global windowopen,w
        print(self.getName()+" is the Monitoring Thread which monitors with Senty the current State")
        while(windowopen):
            result={'Laser - Kodos':{'v':round(np.mean(w.peakKodos), 5),'a':0},
                    'Laser - Kang':{'v':round(np.mean(w.peakKang), 5),'a':0},
                    'Standard Deviation - Kodos':{'v':round(np.sqrt(np.var(w.peakKodos)),5),'a':0},
                    'Standard Deviation - Kang':{'v':round(np.sqrt(np.var(w.peakKang)), 5),'a':0},
                    'Breite - Kodos':{'v':round(np.mean(w.devKodos), 5),'a':0},
                    'Breite - Kang':{'v':round(np.mean(w.devKang), 5),'a':0}
                    }

            timestamp = datetime.datetime.now().strftime("%Y-%m-%d%%20-%%20%H:%M:%S")
            channelData = {}
            i = 0
            for channel in sorted(result):
                i = i+1
                names = channel.split(" - ", 1)
                if len(names) > 1:
                    channelData["name" + str(i)] = names[0] + "." + names[1]
                else:
                    channelData["name" + str(i)] = names[0]
                channelData["value" + str(i)] = str(result[channel]["v"])
                channelData["alert" + str(i)] = str(1 if result[channel]["a"] else 0)
            url = "http://institut2a.physik.rwth-aachen.de/sentry/ag-spin-11/add.php?name0=Timestamp&value0="+timestamp+ "&" + urlencode(channelData)
            requests.get(url, auth=("monitor", "n$limos4")).content
            time.sleep(float(w.holdtime.get()))
            #time.sleep(1)
'''
if __name__ == '__main__':
    vp_start_gui()
    
